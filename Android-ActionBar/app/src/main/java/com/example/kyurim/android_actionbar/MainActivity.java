package com.example.kyurim.android_actionbar;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;


public class MainActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.

    getMenuInflater().inflate(R.menu.menu_main, menu);

    ActionBar actionBar = getSupportActionBar();
//    actionBar.setDisplayShowTitleEnabled(false);
//    actionBar.setDisplayUseLogoEnabled(false);
//    actionBar.setDefaultDisplayHomeAsUpEnabled(false);
//    actionBar.setDefaultDisplayHomeAsUpEnabled(false);
    actionBar.setCustomView(R.layout.actionbar);

    android.support.v7.app.ActionBar.LayoutParams actionBarLayoutParams = new android.support.v7.app.ActionBar
      .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    View customNav = LayoutInflater.from(this).inflate(R.layout.actionbar, null);

    actionBar.setCustomView(customNav, actionBarLayoutParams);
    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

    // show or hide action bar
    //actionBar.hide();
    //actionBar.show();

    // Turn off title from ActionBar
//    actionBar.setDisplayShowTitleEnabled(false);

    // Manually set the ActionBar Title
//    actionBar.setTitle("Title");
//    actionBar.setSubtitle("Subtitle");

    // set background image for ActionBar
//    actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.darkwood_background));
    actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.lightwood_background));

    return true;
  }

//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    // Handle action bar item clicks here. The action bar will
//    // automatically handle clicks on the Home/Up button, so long
//    // as you specify a parent activity in AndroidManifest.xml.
//    int id = item.getItemId();
//
//    //noinspection SimplifiableIfStatement
//    if (id == R.id.action_settings) {
//      return true;
//    }
//
//    return super.onOptionsItemSelected(item);
//  }
}
