package com.example.kyurim.android_listview_image;

// Source for this tutorial:
// https://www.youtube.com/watch?v=WRANgDgM2Zg

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private List<Car> myCars = new ArrayList<Car>();


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    populateCarList();
    populateListView();
    registerClickCallBack();
  }

  // Create Car Objects to be added to ListView.
  private void populateCarList() {
    myCars.add(new Car("Ford", 1940, R.drawable.modelt, "Good condition"));
    myCars.add(new Car("Bugatti", 1943, R.drawable.bugatti, "Excellent"));
    myCars.add(new Car("Corvette", 1977, R.drawable.corvette, "Good condish"));
    myCars.add(new Car("tesla", 2015, R.drawable.tesla, "New"));
    myCars.add(new Car("Volvo", 1955, R.drawable.volvo, "Rusted through"));
  }

  // populate ListView with Car Object.
  private void populateListView() {
    ArrayAdapter<Car> adapter = new MyListAdapter();
    ListView list = (ListView) findViewById(R.id.carsListView);
    list.setAdapter(adapter);
  }

  // display the ListView with Car Object.
  private class MyListAdapter extends ArrayAdapter<Car> {
    public MyListAdapter() {
      super(MainActivity.this,      // which class
        R.layout.item_view,         // which list item
        myCars);                    // what to put into list item
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // make sure we have a view to work with (may have been given null).
      View itemView = convertView;
      if (itemView == null) {
        itemView = getLayoutInflater().inflate(R.layout.item_view, parent, false);   // position is the index of the CarList
      }

      // find the car to work with.
      Car currentCar = myCars.get(position);

      // fill the view.
      ImageView imageView = (ImageView)itemView.findViewById(R.id.item_icon);
      imageView.setImageResource(currentCar.getIconID());

      // Car Model:
      TextView makeText = (TextView) itemView.findViewById(R.id.item_txtMake);
      makeText.setText(currentCar.getMake());

      // Car Year:
      TextView yearText = (TextView) itemView.findViewById(R.id.item_txtYear);
      yearText.setText(""+ currentCar.getYear());

      // Car Condition:
      TextView conditionText = (TextView) itemView.findViewById(R.id.item_txtCondition);
      conditionText.setText(currentCar.getCondition());

      return  itemView;
    }
  }

  // Respond to clicks on ListView
  private void registerClickCallBack() {
    ListView list = (ListView) findViewById(R.id.carsListView);
    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View viewClicked,
                              int position, long id) {
        Car clickedCar = myCars.get(position);
        String message = "You clicked position " + position + " which is car make: " + clickedCar.getMake();
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
      }
    });
  }
}
