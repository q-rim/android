package com.example.kyurim.android_sqlite2;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

// Steps to using the DB:
// 1. Instantiate the DB Adapter
// 2. Open the DB
// 3. use get, insert, delete, ... to change data
// 4. Close the DB

public class MainActivity extends AppCompatActivity {

  DBAdapter myDb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    openDB();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    closeDB();
  }

  private void openDB() {
    myDb = new DBAdapter(this);
    myDb.open();
  }

  private void closeDB() {
    myDb.close();
  }

  private void displayText(String message) {
    TextView textView = (TextView) findViewById(R.id.textDisplay);
    textView.setText(message);
  }



  public void onClick_AddRecord(View v) {
    displayText("Clicked add record");
    long newId = myDb.insertRow("Waldo", 987, "red/white stripe");    // it's a long as it's the id of the record being added.

    // Query for the record we just added.
    // Use the ID:
    Cursor cursor = myDb.getRow(newId);   // getRow() gives a single row of data
    displayRecordSet(cursor);
  }

  public void onClick_ClearAll(View v) {
    displayText("Clicked clear all");
    myDb.deleteAll();
  }

  public void onClick_DisplayRecords(View v) {
    displayText("Clicked display record");
    Cursor cursor = myDb.getAllRows();    // getAllRows() gives all rows of data
    displayRecordSet(cursor);
  }

  private void displayRecordSet(Cursor cursor) {
    String message = "";
    // populate the message from the cursor

    // reset cursor to start, checking to see if there's data:
    if (cursor.moveToFirst()) {
      do {
        int id = cursor.getInt(0);        // column 0
        String name = cursor.getString(1);   // column 1
        int studentNumber = cursor.getInt(2);        // column 2

        // Append data to the message:
        message += "id=" + id +
          ", name=" + name +
          ", student# " + studentNumber + "\n";
      } while (cursor.moveToNext());
    }
    cursor.close();   // close cursor to avoid resource leak.

    displayText(message);
  }
}
