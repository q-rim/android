package com.example.kyurim.android_sqlite1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

  EditText userInput;
  TextView outputText;
  DBHandler dbHandler;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    userInput = (EditText) findViewById(R.id.userInput_editText);
    outputText = (TextView) findViewById(R.id.output_textView);
    dbHandler = new DBHandler(this, null, null, 1);

    printDatabase();
  }

  // Add a product to the database
  public void addButtonClicked(View view) {
    Products product = new Products(userInput.getText().toString());
    dbHandler.addProduct(product);
    printDatabase();
  }

  // Delete items
  public void deleteButtonClicked(View view) {
    String inputText = userInput.getText().toString();
    dbHandler.deleteProduct(inputText);
    printDatabase();
  }

  public void printDatabase() {
    String dbString = dbHandler.databaseToString();
    outputText.setText(dbString);
    userInput.setText("");
  }
}
