package com.example.kyurim.android_listview;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

// Example of implimenting ListView
// 2015.11.07
// Array of options > ArrayAdaptor > Listview
// List view:  { views: res/layout/da_items.xml }

public class MainActivity extends ActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    populateListView();
    registerClickCallback();

  }

  // Implimenting ListView
  private void populateListView() {
    // Create list of items
    String[] myItems = {"Blue", "Green", "Purple", "Red"};

    // Build Adaptor
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
              this,                   // Context for the activity.
              R.layout.da_item,       // Layout to use (create)
              myItems);               // Items to be displayed

    // Configure the listView.
    ListView list = (ListView) findViewById(R.id.listViewMain);
    list.setAdapter(adapter);

  }

  // listen for clicks for ListView.  Respond with toast
  private void registerClickCallback() {
    ListView list = (ListView) findViewById(R.id.listViewMain);
    list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
      @Override
      public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
        TextView textView = (TextView) viewClicked;
        String message = "You clicked # " + position + ", which is string: " + textView.getText().toString();
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
