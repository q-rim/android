package com.example.kyurim.android_sqlite3_listview;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

// Updating the DB entry
// YouTube tutorial (Start: 30:56 sec)
// https://youtu.be/gaOsl2TtMHs?t=30m56s

// Steps to using the DB:
// 1. Instantiate the DB Adapter
// 2. Open the DB
// 3. use get, insert, delete, ... to change data
// 4. Close the DB

public class MainActivity extends AppCompatActivity {

  int[] imageIDs = {  R.drawable.img1,
                      R.drawable.img2,
                      R.drawable.img3,
                      R.drawable.img4,
                      R.drawable.img5,
                      R.drawable.img6,
                    };
  int nextImageIndex = 0;                           // Stopped at YouTube video:  8:23
  DBAdapter myDb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    openDB();
    populateListViewFromDB();
    registerListClickCallback();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    closeDB();
  }

  private void openDB() {
    myDb = new DBAdapter(this);
    myDb.open();
  }

  private void closeDB() {
    myDb.close();
  }

  /*
  * UI Button Callbacks
  */

  public void onClick_AddRecord(View v) {

    int imageId = imageIDs[nextImageIndex];
    nextImageIndex = (nextImageIndex +1) % imageIDs.length;   // increment by one till length of my array.

    // Add it to the DB and re-draw the ListView
    //myDb.insertRow("student name_str", studentID_int, "fav color_str");
    //myDb.insertRow("Jenny", 12345, "Green");
    myDb.insertRow("Jenny" + nextImageIndex, imageId, "Green");
    populateListViewFromDB();               // refresh UI after DB update
  }

  public void onClick_ClearAll(View v) {
    myDb.deleteAll();
    populateListViewFromDB();               // refresh UI after DB update
  }


  private void populateListViewFromDB() {
    Cursor cursor = myDb.getAllRows();

    // Allow activity to manage lifetime of the cursor.
    // DEPRECATED!
    // This runs on the UI Thread.  OK for small/short queries.  - for larger DB list, create a separate thread for this action.
    startManagingCursor(cursor);

    // Setup mapping from cursor to view fields:
    String[] fromFieldNames = new String[]  {DBAdapter.KEY_NAME,      DBAdapter.KEY_STUDENTNUM, DBAdapter.KEY_FAVCOLOUR,      DBAdapter.KEY_STUDENTNUM};
    int[] toViewIDs         = new int[]     {R.id.listview_item_name, R.id.listview_item_icon,  R.id.listview_item_favcolor,  R.id.listview_item_student_id};

    // Create adapter to map columns of the DB onto elements in the UI.
    SimpleCursorAdapter myCursorAdapter =
      new SimpleCursorAdapter(  this,                           // Context
                                R.layout.listview_item_layout,  // Row layout template
                                cursor,                         // cursor
                                fromFieldNames,                 // DB Column names
                                toViewIDs                       // View IDs to put information in
                              );

    // Set the adapter for the listView
    ListView myList = (ListView) findViewById(R.id.listView_fromDB);
    myList.setAdapter(myCursorAdapter);
  }

  // action for clicking on ListView
  private void registerListClickCallback() {
    ListView myList = (ListView) findViewById(R.id.listView_fromDB);
    myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long idInDB) {

        updateItemForId(idInDB);
        displayToastForId(idInDB);
      }
    });
  }

  private void updateItemForId(long idInDB) {
    Cursor cursor = myDb.getRow(idInDB);
    if (cursor.moveToFirst()) {
      // query the DB
      long idDB = cursor.getLong(DBAdapter.COL_ROWID);
      String name = cursor.getString(DBAdapter.COL_NAME);
      int studentNum = cursor.getInt(DBAdapter.COL_STUDENTNUM);
      String favColor = cursor.getString(DBAdapter.COL_FAVCOLOUR);

      favColor += "!";                                            // update the DB content!!!
      myDb.updateRow(idInDB, name, studentNum, favColor);
    }
    cursor.close();
    populateListViewFromDB();
  }

  // Display a toast
  private void displayToastForId(long idInDB) {
    Cursor cursor = myDb.getRow(idInDB);
    if (cursor.moveToFirst()) {
      // query the DB
      long idDB = cursor.getLong(DBAdapter.COL_ROWID);
      String name = cursor.getString(DBAdapter.COL_NAME);
      int studentNum = cursor.getInt(DBAdapter.COL_STUDENTNUM);
      String favColor = cursor.getString(DBAdapter.COL_FAVCOLOUR);

      String message = "ID: " + idDB + "\n"
        + "Name: " + name + "\n"
        + "Student#: " + studentNum + "\n"
        + "FavColour: " + favColor;
      Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }
    cursor.close();
  }
}
